## Lombok Training

The purpose of this repository is to give an overview of all Lombok features by unit testing them.

See the full list of Lombok features here: https://projectlombok.org/features/all

## Tested features

### Stable
- `val`
- `@NonNull`
- `@Cleanup`
- `@Getter/@Setter`
- `@ToString`
- `@EqualsAndHashCode`
- `@NoArgsConstructor`, `@RequiredArgsConstructor` and `@AllArgsConstructor`
- `@Data`
- `@Value`
- `@Builder`
- `@SneakyThrows`
- `@Synchronized`
- `@Getter(lazy=true)`
- `@Log`
### Experimental
- `@Accessors`
- `@ExtensionMethod`
- `@FieldDefaults`
- `@Delegate`
- `@Wither`
- `onMethod= / onConstructor= / onParam=`
- `@UtilityClass`
- `@Helper`
- `@FieldNameConstants`
- `@SuperBuilder`


## Prerequisites
- JDK 1.8+ (check with `java -version`)
- Git 2.0.0+ (check with `git --version`)
- Maven 2.2.1+ (check with `mvn --version`)

## Get started with code
```sh
$ git clone git@bitbucket.org:donmutti-com/training-lombok.git 
```

## Build

```sh
$ cd training-lombok
$ mvn clean
$ mvn package
```
Ensure you see a `BUILD SUCCESS` message.

## Run tests

```sh
$ mvn test
```
Ensure you see a `BUILD SUCCESS` message.

## Troubleshooting

## Community
- Donmutti Team: [hello@donmutti.com](mailto:admin@donmutti.com)
- Donmutti Team Bitbucket: [https://bitbucket.org/donmutti-com](https://bitbucket.org/donmutti-com) 
 

