package com.donmutti.training.lombok.stable.t01_val;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ValTest {

    @Test
    void testVal_Null() {
        val actual = null;
        assertNull(actual);
    }

    @Test
    void testVal_Primitive() {
        val actual = 10;
        assertEquals(10, actual);
    }

    @Test
    void testVal_Reference() {
        val actual = "hello";
        assertEquals("hello", actual);
    }

    @Test
    void testVal_Array() {
        val actual = Stream.of(1, 2, 3).toArray();
        Object[] expected = new Object[]{1, 2, 3};
        assertArrayEquals(expected, actual);
    }

    @Test
    void testVal_List() {
        val actual = Arrays.asList(1, 2, 3);
        List<Integer> expected = Arrays.asList(1, 2, 3);
        assertEquals(expected, actual);
    }

    @Test
    void testVal_Map() {
        val actual = Stream.of(1, 2, 3).collect(toMap(Function.identity(), String::valueOf));
        Map<Integer, Object> expected = Stream.of(1, 2, 3).collect(toMap(Function.identity(), String::valueOf));
        assertEquals(expected, actual);
    }

    @Test
    void testVal_Map_DifferentGenericTypeArguments() {
        val actual = Stream.of(1, 2, 3).collect(toMap(Function.identity(), String::valueOf));
        Map<Integer, Object> expected = Stream.of(1, 2, 3).collect(toMap(Function.identity(), Function.identity()));
        assertThrows(AssertionFailedError.class, () -> assertEquals(expected, actual));
    }
}
