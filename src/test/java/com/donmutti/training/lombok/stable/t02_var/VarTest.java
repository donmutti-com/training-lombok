package com.donmutti.training.lombok.stable.t02_var;

import lombok.val;
import lombok.var;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class VarTest {
    @Test
    void testVar_Null() {
        var actual = (Object)null;
        assertNull(actual);
    }

    @Test
    void testVar_Primitive() {
        var actual = 10;
        assertEquals(10, actual);
    }

    @Test
    void testVar_Primitive_Mutability() {
        var actual = 5;
        assertEquals(5, actual);
        actual = 10;
        assertEquals(10, actual);
    }

    @Test
    void testVar_Reference() {
        var actual = "hello";
        assertEquals("hello", actual);
    }

    @Test
    void testVar_Reference_Mutability() {
        var actual = "hello";
        assertEquals("hello", actual);
        actual = "bye";
        assertEquals("bye", actual);
    }

    @Test
    void testVar_Array() {
        var actual = Stream.of(1, 2, 3).toArray();
        Object[] expected = new Object[]{1, 2, 3};
        assertArrayEquals(expected, actual);
    }

    @Test
    void testVar_List() {
        var actual = Arrays.asList(1, 2, 3);
        List<Integer> expected = Arrays.asList(1, 2, 3);
        assertEquals(expected, actual);
    }

    @Test
    void testVar_Map() {
        var actual = Stream.of(1, 2, 3).collect(toMap(Function.identity(), String::valueOf));
        Map<Integer, Object> expected = Stream.of(1, 2, 3).collect(toMap(Function.identity(), String::valueOf));
        assertEquals(expected, actual);
    }

    @Test
    void testVar_Map_DifferentGenericTypeArguments() {
        var actual = Stream.of(1, 2, 3).collect(toMap(Function.identity(), String::valueOf));
        Map<Integer, Object> expected = Stream.of(1, 2, 3).collect(toMap(Function.identity(), Function.identity()));
        assertThrows(AssertionFailedError.class, () -> assertEquals(expected, actual));
    }
}
