package com.donmutti.training.lombok.stable.t04_Cleanup;

import lombok.Cleanup;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


class CleanMeUp {
    private boolean active = true;

    public void cleanUp() {
        active = false;
    }

    public boolean isActive() {
        return active;
    }

}

public class CleanupTest {

    @Test
    void testCleanup() throws IOException {
        StringReader outer;
        {
            @Cleanup StringReader inner = new StringReader("hello");
            outer = inner;
            assertTrue(outer.ready());
        }
        assertThrows(IOException.class, () -> outer.ready());
    }

    @Test
    void testCleanup_CustomMethod() {
        CleanMeUp outer;
        {
            @Cleanup("cleanUp") CleanMeUp inner = new CleanMeUp();
            outer = inner;
            assertTrue(outer.isActive());
        }
        assertFalse(outer.isActive());
    }
}