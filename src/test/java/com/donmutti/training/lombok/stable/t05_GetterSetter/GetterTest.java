package com.donmutti.training.lombok.stable.t05_GetterSetter;

import lombok.Getter;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

@Getter
class ClassGetter {
    private Integer field = 15;
}

class FieldGetter {
    @Getter
    private Integer field = 15;
}

public class GetterTest {

    @Test
    void testClassGetter() {
        testInstance(new ClassGetter());
    }

    @Test
    void testFieldGetters() {
        testInstance(new FieldGetter());
    }

    private void testInstance(Object instance) {
        Class<?> clazz = instance.getClass();
        Method method = null;
        try {
            method = clazz.getDeclaredMethod("getField");
            assertNotNull(method);
            assertEquals(Integer.class, method.getReturnType());
            assertEquals(15, method.invoke(instance));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            fail();
        }
    }
}
