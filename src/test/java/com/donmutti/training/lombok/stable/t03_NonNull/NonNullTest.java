package com.donmutti.training.lombok.stable.t03_NonNull;

import lombok.NonNull;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class Echo {
    static <T> T tryEcho(@NonNull T input) {
        return input;
    }
}

public class NonNullTest {

    @Test
    void testNonNull() {
        assertEquals("hello", Echo.tryEcho("hello"));
    }

    @Test
    void testNonNull_Null() {
        Throwable expected = assertThrows(NullPointerException.class, () -> Echo.tryEcho(null));
        assertEquals("input is marked @NonNull but is null", expected.getMessage());
    }

}
